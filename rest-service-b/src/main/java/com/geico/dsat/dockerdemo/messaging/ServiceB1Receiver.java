/*
 * Copyright 2015 Sean Brandt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.geico.dsat.dockerdemo.messaging;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.geico.dsat.dockerdemo.service.ServiceB1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;

/**
 * @since 11/30/15
 */
@Component
@EnableBinding(Sink.class)
public class ServiceB1Receiver {

    private static final Logger log = LoggerFactory.getLogger(ServiceB1Receiver.class);

    @Autowired
    private ServiceB1 serviceB1;

    @Autowired
    private ObjectMapper objectMapper;

    @ServiceActivator(inputChannel = "ServiceB1")
    public void receiveMessage(String message) throws IOException {
        log.info("Received message: << {} >>", message);
        JsonNode jsonData = objectMapper.reader().readTree(message);
        Instant time;
        if (jsonData.has("plusDays")) {
            time = serviceB1.getTimePlusDays(jsonData.get("plusDays").asInt());
        } else {
            time = serviceB1.getTime();
        }
        log.info("ServiceA1 result: << {}",time);
    }

}
