/*
 * Copyright 2015 Sean Brandt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.geico.dsat.dockerdemo.messaging

import com.fasterxml.jackson.databind.ObjectMapper
import com.geico.dsat.dockerdemo.domain.ServiceA1DomainData
import com.geico.dsat.dockerdemo.service.ServiceA1
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.cloud.stream.messaging.Sink
import org.springframework.integration.annotation.ServiceActivator
import org.springframework.stereotype.Component

/**
 * @since 11/22/15
 */
@Slf4j
@Component
@EnableBinding(Sink)
class ServiceA1Receiver {
    @Autowired
    ServiceA1 serviceA1
    @Autowired
    ObjectMapper objectMapper

    @ServiceActivator(inputChannel = "ServiceA1")
    def receiveMessage(String message) {
        log.info "Received message: << $message >>"
        def jsonData = objectMapper.reader().readTree(message)
        def result = serviceA1.getData(jsonData.get("name").asText())
        log.info "ServiceA1 result: << ${objectMapper.writerFor(ServiceA1DomainData).writeValueAsString(result)}"
    }
}
