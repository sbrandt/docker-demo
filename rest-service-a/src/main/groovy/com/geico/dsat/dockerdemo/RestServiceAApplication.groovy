package com.geico.dsat.dockerdemo

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.DiscoveryClient
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.context.annotation.Bean

@SpringBootApplication
@EnableDiscoveryClient
class RestServiceAApplication {

    @Autowired
    private DiscoveryClient discoveryClient

    @Bean
    ObjectMapper objectMapper() { new ObjectMapper() }

    static void main(String[] args) {
        SpringApplication.run RestServiceAApplication, args
    }
}
