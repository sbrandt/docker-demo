package com.geico.dsat.dockerdemo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.DiscoveryClient
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@SpringBootApplication
@EnableDiscoveryClient
class RouterApplication {

    @Autowired
    private DiscoveryClient discoveryClient

    static void main(String[] args) {
        SpringApplication.run RouterApplication, args
    }
}
